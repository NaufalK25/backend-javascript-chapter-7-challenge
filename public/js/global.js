const alertFlash = document.querySelector('div#alertFlash');

setTimeout(() => {
    alertFlash?.remove();
}, 5000);
