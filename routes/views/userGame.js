const express = require('express');
const { body, param } = require('express-validator');
const passport = require('../../middlewares/passportLocal');
const { methodNotAllowedPage } = require('../../controllers/views/error');
const { createUserGame, deleteUserGameById, getAllUserGamesPage, getUserGameByIdPage, updateUserGameById } = require('../../controllers/views/userGame');
const { UserGame } = require('../../database/models');
const { getDataBySpecificField } = require('../../helper');

const router = express.Router();

router.route('/user_games')
    .get((req, res, next) => {
        if (!req.user) return res.redirect('/view/login');
        next();
    }, getAllUserGamesPage)
    .post([
        body('username')
            .trim()
            .notEmpty().withMessage('Username is required')
            .isString().withMessage('Username must be a string')
            .isLength({ min: 3 }).withMessage('Username must be at least 3 characters long')
            .custom(async value => {
                const userGame = await getDataBySpecificField(UserGame, 'username')(value);
                if (userGame) throw new Error('Username already exists');
            }),
        body('password')
            .notEmpty().withMessage('Password is required')
            .isString().withMessage('Password must be a string')
            .isLength({ min: 6 }).withMessage('Password must be at least 6 characters long')
    ], createUserGame)
    .all(methodNotAllowedPage);

router.route('/user_game/:id')
    .get((req, res, next) => {
        if (!req.user) return res.redirect('/view/login');
        next();
    }, [
        param('id').isInt().withMessage('Id must be an integer')
    ], getUserGameByIdPage)
    .patch([
        body('username')
            .trim()
            .notEmpty().withMessage('Username is required')
            .isString().withMessage('Username must be a string')
            .isLength({ min: 3 }).withMessage('Username must be at least 3 characters long')
            .custom(async (value) => {
                const userGame = await getDataBySpecificField(UserGame, 'username')(value);
                if (userGame) throw new Error('Username already exists');
            }),
        body('password')
            .notEmpty().withMessage('Password is required')
            .isString().withMessage('Password must be a string')
            .isLength({ min: 6 }).withMessage('Password must be at least 6 characters long')
    ], updateUserGameById)
    .delete([
        param('id').isInt().withMessage('Id must be an integer')
    ], deleteUserGameById)
    .all(methodNotAllowedPage);

module.exports = router;
