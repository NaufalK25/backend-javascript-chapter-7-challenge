const express = require('express');
const { body } = require('express-validator');
const { methodNotAllowedPage } = require('../../controllers/views/error');
const { getLoginPage, getRegisterPage, login, logout, register } = require('../../controllers/views/auth');
const { UserGame } = require('../../database/models');
const { getDataBySpecificField } = require('../../helper');

const router = express.Router();

router.route('/register')
    .get((req, res, next) => {
        if (req.user) return res.redirect('/view/user_games');
        next();
    }, getRegisterPage)
    .post([
        body('username')
            .trim()
            .notEmpty().withMessage('Username is required')
            .isString().withMessage('Username must be a string')
            .isLength({ min: 3 }).withMessage('Username must be at least 3 characters long')
            .custom(async value => {
                const userGame = await getDataBySpecificField(UserGame, 'username')(value);
                if (userGame) throw new Error('Username already exists');
            }),
        body('password')
            .trim()
            .notEmpty().withMessage('Password is required')
            .isString().withMessage('Password must be a string')
            .isLength({ min: 6 }).withMessage('Password must be at least 6 characters long'),
            body('confirmPassword')
            .trim()
            .notEmpty().withMessage('Confirm password is required')
            .isString().withMessage('Password must be a string')
    ], register)
    .all(methodNotAllowedPage);

router.route('/login')
    .get((req, res, next) => {
        if (req.user) return res.redirect('/view/user_games');
        next();
    }, getLoginPage)
    .post([
        body('username')
            .trim()
            .notEmpty().withMessage('Username is required')
            .isString().withMessage('Username must be a string')
            .isLength({ min: 3 }).withMessage('Username must be at least 3 characters long'),
        body('password')
            .trim()
            .notEmpty().withMessage('Password is required')
            .isString().withMessage('Password must be a string')
            .isLength({ min: 6 }).withMessage('Password must be at least 6 characters long')
    ], login)
    .all(methodNotAllowedPage);

router.route('/logout')
    .post((req, res, next) => {
        if (!req.user) return res.redirect('/view/login');
        next();
    }, logout)
    .all(methodNotAllowedPage);

module.exports = router;
