const bcrypt = require('bcrypt');
const { validationResult } = require('express-validator');
const passport = require('../../middlewares/passportLocal');
const { internalServerErrorPage } = require('./error');
const { UserGame } = require('../../database/models');
const { generateFlash, generateFlashObject, generateRenderObject, getDataBySpecificField } = require('../../helper');

const getUserGameByUsername = getDataBySpecificField(UserGame, 'username');
const generateAuthRenderObject = (req, title) => {
    return generateRenderObject({
        title,
        scripts: ['../../js/global.js'],
        extras: { flash: generateFlashObject(req) }
    });
}

module.exports = {
    getLoginPage: (req, res) => {
        try {
            res.status(200).render('login', generateAuthRenderObject(req, 'Login'));
        } catch (error) {
            internalServerErrorPage(error, req, res);
        }
    },
    getRegisterPage: (req, res) => {
        try {
            res.status(200).render('register', generateAuthRenderObject(req, 'Register'));
        } catch (error) {
            internalServerErrorPage(error, req, res);
        }
    },
    logout: async (req, res, next) => {
        req.logout((err) => {
            if (err) return next(err);
            generateFlash(req, { type: 'success', message: 'You have been logged out' });
            res.redirect('/view/login');
        });
    },
    login: (req, res) => {
        passport.authenticate('local', (err, user, info) => {
            if (err) return internalServerErrorPage(err, req, res);
            if (!user) {
                generateFlash(req, { type: 'danger', errors: [{ param: info.param, msg: info.message }] });
                return res.status(400).render('login', generateAuthRenderObject(req, 'Login'));
            }

            req.login(user, (err) => {
                if (err) return internalServerErrorPage(err, req, res);

                const errors = validationResult(req);

                if (!errors.isEmpty()) {
                    generateFlash(req, { type: 'danger', message: errors.array()[0].msg });
                    return res.status(400).render('login', generateAuthRenderObject(req, 'Login'));
                }

                generateFlash(req, { type: 'success', message: 'Login successful' });
                res.redirect('/view/user_games');
            });
        })(req, res);
    },
    register: async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                generateFlash(req, { type: 'danger', errors: errors.array() });
                return res.status(400).render('register', generateAuthRenderObject(req, 'Register'));
            }

            if (req.body.password !== req.body.confirmPassword) {
                generateFlash(req, { type: 'danger', errors: [{ param: 'confirmPassword', msg: 'Password confirmation does not match' }] })
                return res.status(400).render('register', generateAuthRenderObject(req, 'Register'));
            }

            await UserGame.create({
                username: req.body.username,
                password: await bcrypt.hash(req.body.password, 10)
            });

            generateFlash(req, { type: 'success', message: 'Your account has been registered' });
            res.status(201).redirect('/view/login');
        } catch (error) {
            internalServerErrorPage(error, req, res);
        }
    }
}
