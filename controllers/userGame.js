const bcrypt = require('bcrypt');
const { validationResult } = require('express-validator');
const { badRequest, notFound } = require('./error');
const { baseUrl } = require('../config/constants');
const { UserGame, UserGameBiodata, UserGameHistory } = require('../database/models');
const { getDataBySpecificField } = require('../helper');

const getUserGameById = getDataBySpecificField(UserGame, 'id');
const jsonProfilePicturePath = `${baseUrl}/uploads/profiles/`;
const jsonGameCoverPath = `${baseUrl}/uploads/games/`;

module.exports = {
    create: async (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) return badRequest(errors.array(), req, res);

        const userGame = await UserGame.create({
            username: req.body.username,
            password: await bcrypt.hash(req.body.password, 10)
        });

        res.status(201).json({
            statusCode: 201,
            message: 'UserGame created successfully',
            data: userGame
        });
    },
    update: async (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) return badRequest(errors.array(), req, res);

        const userGame = await getUserGameById(req.params.id);

        if (!userGame) return notFound(req, res);

        const oldUserGameData = { ...userGame.dataValues };
        const userGameFields = Object.keys(userGame.dataValues);
        let fieldChanged = Object.keys(req.body).filter(field => userGameFields.includes(field));
        const before = {}, after = {};

        fieldChanged.forEach(async field => {
            before[field] = oldUserGameData[field];
            if (field === 'password') {
                const hashedPassword = bcrypt.hashSync(req.body.password, 10);
                after[field] = hashedPassword;
            } else {
                after[field] = req.body[field];
            }
        });

        await userGame.update(after);

        res.status(200).json({
            statusCode: 200,
            message: 'UserGame updated successfully',
            data: { before, after }
        });
    },
    destroy: async (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) return badRequest(errors.array(), req, res);

        const userGame = await getUserGameById(req.params.id);
        const userGameBiodata = await getDataBySpecificField(UserGameBiodata, 'userGameId')(req.params.id);
        const userGameHistories = await UserGameHistory.findAll({ where: { userGameId: req.params.id } });

        if (!userGame) return notFound(req, res);

        userGameHistories.forEach(async userGameHistory => await userGameHistory?.destroy());
        await userGameBiodata?.destroy();
        await userGame.destroy();

        res.status(200).json({
            statusCode: 200,
            message: 'UserGame deleted successfully',
            data: userGame
        });
    },
    findOne: async (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) return badRequest(errors.array(), req, res);

        const userGame = await getUserGameById(req.params.id, [
            { model: UserGameBiodata },
            { model: UserGameHistory }
        ]);

        if (!userGame) return notFound(req, res);

        if (userGame.UserGameBiodatum) {
            const profilePicture = userGame.UserGameBiodatum.dataValues.profilePicture;
            userGame.UserGameBiodatum.dataValues.profilePicture = `${jsonProfilePicturePath}${profilePicture}`;
        }
        userGame.UserGameHistories.forEach(userGameHistory => {
            const gameCover = userGameHistory.dataValues.cover;
            userGameHistory.dataValues.cover = `${jsonGameCoverPath}${gameCover}`;
        });

        res.status(200).json({
            statusCode: 200,
            message: 'OK',
            data: userGame
        });
    },
    findAll: async (req, res) => {
        const userGames = await UserGame.findAll({
            include: [
                { model: UserGameBiodata },
                { model: UserGameHistory }
            ]
        });

        userGames.forEach(userGame => {
            if (userGame.UserGameBiodatum) {
                const profilePicture = userGame.UserGameBiodatum.dataValues.profilePicture;
                userGame.UserGameBiodatum.dataValues.profilePicture = `${jsonProfilePicturePath}${profilePicture}`;
            }
            userGame.UserGameHistories.forEach(userGameHistory => {
                const gameCover = userGameHistory.dataValues.cover;
                userGameHistory.dataValues.cover = `${jsonGameCoverPath}${gameCover}`;
            });

        });

        res.status(200).json({
            statusCode: 200,
            message: 'OK',
            count: userGames.length,
            data: userGames
        });
    }
}
