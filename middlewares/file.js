const path = require('path');
const multer = require('multer');

const randomBetween = (min = 15, max = 20) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const randomFileName = (length = 10) => {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    return Array.from({ length }, () => chars[Math.floor(Math.random() * chars.length)]).join('');
}

const createStorage = (uploadField = '') => {
    return multer.diskStorage({
        destination: (req, file, cb) => {
            if (file.mimetype.startsWith('image/')) {
                cb(null, `uploads/${uploadField}`);
            } else {
                cb(new Error('Invalid image type'), null);
            }
        },
        filename: (req, file, cb) => {
            if (file.mimetype.startsWith('image/')) {
                cb(null, `${randomFileName(randomBetween())}${path.extname(file.originalname)}`);
            } else {
                cb(new Error('Invalid image type'), null);
            }
        }
    });
}

const profileStorage = createStorage('profiles');
const gameStorage = createStorage('games');

module.exports = {
    randomBetween,
    randomFileName,
    createStorage,
    gameStorage,
    profileStorage
};
