const multer = 'multer';
const { testingTimeout } = require('../config/constants');
const { createStorage, randomBetween, randomFileName } = require('../middlewares/file');

process.env.NODE_ENV = 'test';
jest.setTimeout(testingTimeout);

describe('createStorage function', () => {
    it('should return a storage object', done => {
        const storage = createStorage('profiles');
        expect(storage).toBeInstanceOf(Object);
        expect(storage).toBeTruthy();
        done();
    });
    it('no arg(s)', done => {
        expect(createStorage()).toBeInstanceOf(Object);
        expect(createStorage()).toBeTruthy();
        done();
    });
});

describe('randomBetween function', () => {
    it('shoulr return a number', done => {
        expect(typeof randomBetween()).toBe('number');
        expect(typeof randomBetween(20, 25)).toBe('number');
        done();
    });
    it('should return a number between specified min and max', done => {
        expect(randomBetween()).toBeGreaterThanOrEqual(15);
        expect(randomBetween()).toBeLessThanOrEqual(20);

        const random = randomBetween(20, 25);
        expect(random).toBeGreaterThanOrEqual(20);
        expect(random).toBeLessThanOrEqual(25);
        done();
    });
});

describe('randomFileName function', () => {
    it('should return a string', done => {
        expect(typeof randomFileName()).toBe('string');
        expect(typeof randomFileName(10)).toBe('string');
        done();
    });
    it('should return a string with specified length', done => {
        expect(randomFileName()).toHaveLength(10);
        expect(randomFileName(15)).toHaveLength(15);
        done();
    });
});
