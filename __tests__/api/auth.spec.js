const request = require('supertest');
const app = require('../../app');
const { testingTimeout } = require('../../config/constants');
const { UserGame } = require('../../database/models');
const { getDataBySpecificField, getEndpoint } = require('../../helper');

process.env.NODE_ENV = 'test';
jest.setTimeout(testingTimeout);

describe('POST /login', () => {
    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'loginsuccess',
            password: 'loginsuccess',
        });
        const res = await request(app).post('/api/v1/login').send({
            username: 'loginsuccess',
            password: 'loginsuccess',
        });
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('Logged in successfully');
        expect(res.body).toHaveProperty('token');
        expect(typeof res.body.token).toBe('string');

        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request (Empty Body)', async () => {
        const res = await request(app).post('/api/v1/login').send({});
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (Username Not Found)', async () => {
        const res = await request(app).post('/api/v1/login').send({
            username: 'loginusernamenotfound',
            password: 'loginusernamenotfound'
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (Incorrect Password)', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'loginincorrectpassword',
            password: 'loginincorrectpassword'
        });
        const res = await request(app).post('/api/v1/login').send({
            username: 'loginincorrectpassword',
            password: '123456'
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }

        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
});

describe('/login Method Not Allowed', () => {
    it('GET /login', async () => {
        const res = await request(app).get('/api/v1/login');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method GET not allowed at endpoint ${getEndpoint('/api/v1/login')}`);
    });
    it('PUT /login', async () => {
        const res = await request(app).put('/api/v1/login');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/login')}`);
    });
    it('PATCH /login', async () => {
        const res = await request(app).patch('/api/v1/login');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/login')}`);
    });
    it('DELETE /login', async () => {
        const res = await request(app).delete('/api/v1/login');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/login')}`);
    });
});

describe('POST /register', () => {
    it('200 OK', async () => {
        const res = await request(app).post('/api/v1/register').send({
            username: 'register',
            password: 'register'
        });
        const userGame = await getDataBySpecificField(UserGame, 'username')('register');
        expect(res.statusCode).toBe(201);
        expect(res.body.statusCode).toBe(201);
        expect(res.body.message).toBe('Registered successfully');

        const responseData = res.body.data;
        for (const key in userGame.dataValues) {
            expect(responseData).toHaveProperty(key);
            expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGame.dataValues[key]));
        }

        await request(app).delete(`/api/v1/user_game/${responseData.id}`);
    });
    it('400 Bad Request (Empty Body)', async () => {
        const res = await request(app).post('/api/v1/register').send({});
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (Validation Failed)', async () => {
        const res = await request(app).post('/api/v1/register').send({
            username: 'jh',
            password: '123'
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
});

describe('/register Method Not Allowed', () => {
    it('GET /register', async () => {
        const res = await request(app).get('/api/v1/register');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method GET not allowed at endpoint ${getEndpoint('/api/v1/register')}`);
    });
    it('PUT /register', async () => {
        const res = await request(app).put('/api/v1/register');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/register')}`);
    });
    it('PATCH /register', async () => {
        const res = await request(app).patch('/api/v1/register');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/register')}`);
    });
    it('DELETE /register', async () => {
        const res = await request(app).delete('/api/v1/register');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/register')}`);
    });
});

