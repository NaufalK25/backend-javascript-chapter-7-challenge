const request = require('supertest');
const app = require('../../app');
const { baseUrl, testingTimeout } = require('../../config/constants');
const { UserGame, UserGameHistory } = require('../../database/models');
const { getDataBySpecificField, getEndpoint } = require('../../helper');

process.env.NODE_ENV = 'test';
jest.setTimeout(testingTimeout);
const jsonGameCoverPath = `${baseUrl}/uploads/games/`;

describe('GET /user_games/histories', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameHistories = await UserGameHistory.findAll({ include: [{ model: UserGame }] });
        const res = await request(app).get('/api/v1/user_games/histories');
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');
        expect(res.body.count).toBe(userGameHistories.length);

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Array);
        expect(responseData).toHaveLength(userGameHistories.length);
        for (let i = 0; i < userGameHistories.length; i++) {
            for (const key in userGameHistories[i].dataValues) {
                expect(responseData[i]).toHaveProperty(key);
                if (key === 'cover') {
                    expect(JSON.stringify(responseData[i][key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGameHistories[i].dataValues[key]}`));
                } else {
                    expect(JSON.stringify(responseData[i][key])).toBe(JSON.stringify(userGameHistories[i].dataValues[key]));
                }

                const userGame = responseData[i].UserGame;
                if (userGame) {
                    expect(userGame).toBeInstanceOf(Object);
                    for (const key in userGameHistories[i].UserGame.dataValues) {
                        expect(userGame).toHaveProperty(key);
                        expect(JSON.stringify(userGame[key])).toBe(JSON.stringify(userGameHistories[i].UserGame.dataValues[key]));
                    }
                } else {
                    expect(userGame).toBeNull();
                }
            }
        }
    });
});

describe('POST /user_games/histories', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('201 Created', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamehistorycreate',
            password: 'usergameusergamehistorycreate'
        });
        const userGameId = userGameResponse.body.data.id;
        const res = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistorycreate',
            publisher: 'usergamehistorycreate',
            score: 100,
            userGameId
        });
        expect(res.statusCode).toBe(201);
        expect(res.body.statusCode).toBe(201);
        expect(res.body.message).toBe('UserGameHistory created successfully');

        const responseData = res.body.data;
        const userGameHistory = await getDataBySpecificField(UserGameHistory, 'title')(randomTitle);
        for (const key in userGameHistory.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'cover') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGameHistory.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameHistory.dataValues[key]));
            }
        }

        await request(app).delete(`/api/v1/user_game/history/${responseData.id}`)
        await request(app).delete(`/api/v1/user_game/${userGameId}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).post('/api/v1/user_games/histories').send({
            title: '',
            publisher: '',
            score: '100',
            userGameId: 1
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
});

describe('/user_games/histories Method Not Allowed', () => {
    it('PUT /user_games/histories', async () => {
        const res = await request(app).put('/api/v1/user_games/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_games/histories')}`);
    });
    it('PATCH /user_games/histories', async () => {
        const res = await request(app).patch('/api/v1/user_games/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/user_games/histories')}`);
    });
    it('DELETE /user_games/histories', async () => {
        const res = await request(app).delete('/api/v1/user_games/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/user_games/histories')}`);
    });
});

describe('GET /user_game/history/:id', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamehistoryget',
            password: 'usergameusergamehistoryget'
        });
        const userGameHistoryResponse = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistoryget',
            publisher: 'usergamehistoryget',
            score: 100,
            userGameId: userGameResponse.body.data.id
        });
        const res = await request(app).get(`/api/v1/user_game/history/${userGameHistoryResponse.body.data.id}`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        const userGameHistory = await getDataBySpecificField(UserGameHistory, 'id')(1, [{ model: UserGame }]);
        for (const key in userGameHistory.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'cover') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGameHistory.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameHistory.dataValues[key]));
            }

            const userGame = responseData.UserGame;
            if (userGame) {
                expect(userGame).toBeInstanceOf(Object);
                for (const key in userGameHistory.UserGame.dataValues) {
                    expect(userGame).toHaveProperty(key);
                    expect(JSON.stringify(userGame[key])).toBe(JSON.stringify(userGameHistory.UserGame.dataValues[key]));
                }
            } else {
                expect(userGame).toBeNull();
            }
        }

        await request(app).delete(`/api/v1/user_game/history/${responseData.id}`)
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).get('/api/v1/user_game/history/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('404 Not Found', async () => {
        const res = await request(app).get('/api/v1/user_game/history/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/history/0')} not found`);
    });
});

describe('PATCH /user_game/history/:id', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamehistoryupdate',
            password: 'usergameusergamehistoryupdate'
        });
        const userGameHistoryResponse = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistoryupdate',
            publisher: 'usergamehistoryupdate',
            score: 100,
            userGameId: userGameResponse.body.data.id
        });
        const updatedData = {
            title: 'GTA V',
            score: 80
        };
        const userGameHistory = await getDataBySpecificField(UserGameHistory, 'id')(userGameHistoryResponse.body.data.id);
        const res = await request(app).patch(`/api/v1/user_game/history/${userGameHistoryResponse.body.data.id}`).send(updatedData);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('UserGameHistory updated successfully');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        expect(responseData).toHaveProperty('before');
        expect(responseData).toHaveProperty('after');
        for (const key of Object.keys(updatedData)) {
            expect(responseData.before).toHaveProperty(key);
            expect(responseData.after).toHaveProperty(key);
            if (key === 'cover') {
                expect(responseData.before[key]).toBe(`${jsonGameCoverPath}${userGameHistory.dataValues[key]}`);
                expect(responseData.after[key]).toBe(`${jsonGameCoverPath}${updatedData[key]}`);
            } else {
                expect(responseData.before[key]).toBe(userGameHistory[key]);
                expect(responseData.after[key]).toBe(updatedData[key]);
            }
        }

        await request(app).delete(`/api/v1/user_game/history/${responseData.id}`)
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request (param)', async () => {
        const res = await request(app).patch('/api/v1/user_game/history/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (body)', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamehistoryupdatebadrequestbody',
            password: 'usergameusergamehistoryupdatebadrequestbody'
        });
        const userGameHistoryResponse = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistoryupdatebadrequestbody',
            publisher: 'usergamehistoryupdatebadrequestbody',
            score: 100,
            userGameId: userGameResponse.body.data.id
        });
        const res = await request(app).patch(`/api/v1/user_game/history/${userGameHistoryResponse.body.data.id}`).send({ score: 'a' })
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }

        await request(app).delete(`/api/v1/user_game/history/${userGameHistoryResponse.body.data.id}`)
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('404 Not Found', async () => {
        const res = await request(app).patch('/api/v1/user_game/history/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/history/0')} not found`);
    });
});

describe('DELETE /user_game/history/:id', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamehistorydelete',
            password: 'usergameusergamehistorydelete'
        });
        const userGameHistoryResponse = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistorydelete',
            publisher: 'usergamehistorydelete',
            score: 100,
            userGameId: userGameResponse.body.data.id
        });
        const userGameHistory = await getDataBySpecificField(UserGameHistory, 'id')(userGameResponse.body.data.id);
        const res = await request(app).delete(`/api/v1/user_game/history/${userGameHistoryResponse.body.data.id}`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('UserGameHistory deleted successfully');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        for (const key in userGameHistory.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'cover') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGameHistory.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameHistory.dataValues[key]));
            }
        }

        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).delete('/api/v1/user_game/history/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('404 Not Found', async () => {
        const res = await request(app).delete('/api/v1/user_game/history/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/history/0')} not found`);
    });
});

describe('/user_game/history/:id Method Not Allowed', () => {
    it('POST /user_game/history/:id', async () => {
        const res = await request(app).post('/api/v1/user_game/history/1');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method POST not allowed at endpoint ${getEndpoint('/api/v1/user_game/history/1')}`);
    });
    it('PUT /user_game/history/:id', async () => {
        const res = await request(app).put('/api/v1/user_game/history/1');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_game/history/1')}`);
    });
});

describe('GET /user_game/:userGameId/histories', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamehistorygetusergameid',
            password: 'usergameusergamehistorygetusergameid'
        });
        const userGameHistory1Response = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistorygetusergameid1',
            publisher: 'usergamehistorygetusergameid1',
            score: 100,
            userGameId: userGameResponse.body.data.id
        });
        const userGameHistory2Response = await request(app).post('/api/v1/user_games/histories').send({
            title: 'usergamehistorygetusergameid2',
            publisher: 'usergamehistorygetusergameid2',
            score: 100,
            userGameId: userGameResponse.body.data.id
        });
        const userGameHistories = await UserGameHistory.findAll({ where: { userGameId: userGameResponse.body.data.id } });
        const res = await request(app).get(`/api/v1/user_game/${userGameResponse.body.data.id}/histories`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');
        expect(res.body.data).toBeInstanceOf(Array);
        expect(res.body.data.length).toBe(userGameHistories.length);

        for (let i = 0; i < userGameHistories.length; i++) {
            const userGameHistory = userGameHistories[i];
            const responseData = res.body.data[i];
            expect(responseData).toBeInstanceOf(Object);
            for (const key in userGameHistory.dataValues) {
                expect(responseData).toHaveProperty(key);
                if (key === 'cover') {
                    expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGameHistory.dataValues[key]}`));
                } else {
                    expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameHistory.dataValues[key]));
                }
            }
        }

        await request(app).delete(`/api/v1/user_game/history/${userGameHistory1Response.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/history/${userGameHistory2Response.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).get('/api/v1/user_game/a/histories');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
});

describe('/user_game/:userGameId/history Method Not Allowed', () => {
    it('POST /user_game/:userGameId/histories', async () => {
        const res = await request(app).post('/api/v1/user_game/1/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method POST not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/histories')}`);
    });
    it('PUT /user_game/:userGameId/histories', async () => {
        const res = await request(app).put('/api/v1/user_game/1/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/histories')}`);
    });
    it('PATCH /user_game/:userGameId/histories', async () => {
        const res = await request(app).patch('/api/v1/user_game/1/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/histories')}`);
    });
    it('DELETE /user_game/:userGameId/histories', async () => {
        const res = await request(app).delete('/api/v1/user_game/1/histories');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/histories')}`);
    });
});
