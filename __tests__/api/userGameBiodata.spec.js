const request = require('supertest');
const app = require('../../app');
const { baseUrl, testingTimeout } = require('../../config/constants');
const { UserGame, UserGameBiodata } = require('../../database/models');
const { getDataBySpecificField, getEndpoint } = require('../../helper');

process.env.NODE_ENV = 'test';
jest.setTimeout(testingTimeout);
const jsonProfilePicturePath = `${baseUrl}/uploads/profiles/`;

describe('GET /user_games/biodatas', () => {
    beforeEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameBiodatas = await UserGameBiodata.findAll({ include: [{ model: UserGame }] });
        const res = await request(app).get('/api/v1/user_games/biodatas');
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');
        expect(res.body.count).toBe(userGameBiodatas.length);

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Array);
        expect(responseData).toHaveLength(userGameBiodatas.length);
        for (let i = 0; i < userGameBiodatas.length; i++) {
            for (const key in userGameBiodatas[i].dataValues) {
                expect(responseData[i]).toHaveProperty(key);
                if (key === 'profilePicture') {
                    expect(JSON.stringify(responseData[i][key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGameBiodatas[i].dataValues[key]}`));
                } else {
                    expect(JSON.stringify(responseData[i][key])).toBe(JSON.stringify(userGameBiodatas[i].dataValues[key]));
                }

                const userGame = responseData[i].UserGame;
                if (userGame) {
                    expect(userGame).toBeInstanceOf(Object);
                    for (const key in userGameBiodatas[i].UserGame.dataValues) {
                        expect(userGame).toHaveProperty(key);
                        expect(JSON.stringify(userGame[key])).toBe(JSON.stringify(userGameBiodatas[i].UserGame.dataValues[key]));
                    }
                } else {
                    expect(userGame).toBeNull();
                }
            }
        }
    });
});

describe('POST /user_games/biodatas', () => {
    beforeEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('201 Created', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamebiodatacreate',
            password: 'usergameusergamebiodatacreate'
        });
        const userGameId = userGameResponse.body.data.id;
        const res = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'usergamebiodatacreate@gmail.com',
            firstname: 'UserGame',
            lastname: 'BiodataCreate',
            country: 'USA',
            age: 20,
            userGameId
        });
        expect(res.statusCode).toBe(201);
        expect(res.body.statusCode).toBe(201);
        expect(res.body.message).toBe('UserGameBiodata created successfully');

        const responseData = res.body.data;
        const userGameBiodata = await getDataBySpecificField(UserGameBiodata, 'email')('usergamebiodatacreate@gmail.com');
        for (const key in userGameBiodata.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'profilePicture') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGameBiodata.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameBiodata.dataValues[key]));
            }
        }

        await request(app).delete(`/api/v1/user_game/biodata/${responseData.id}`);
        await request(app).delete(`/api/v1/user_game/${userGameId}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).post('/api/v1/user_games/biodatas').send({
            email: '',
            firstname: 0,
            country: true,
            age: '20',
            userGameId: false
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (Email Already Exists)', async () => {
        const res = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'johntest@gmail.com',
            firstname: 'John',
            country: 'Test',
            age: 25,
            userGameId: 1
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
});

describe('/user_games/biodatas Method Not Allowed', () => {
    it('PUT /user_games/biodatas', async () => {
        const res = await request(app).put('/api/v1/user_games/biodatas');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_games/biodatas')}`);
    });
    it('PATCH /user_games/biodatas', async () => {
        const res = await request(app).patch('/api/v1/user_games/biodatas');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/user_games/biodatas')}`);
    });
    it('DELETE /user_games/biodatas', async () => {
        const res = await request(app).delete('/api/v1/user_games/biodatas');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/user_games/biodatas')}`);
    });
});

describe('GET /user_game/biodata/:id', () => {
    beforeEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: `usergameusergamebiodataget`,
            password: 'usergameusergamebiodataget'
        });
        const userGameBiodataResponse = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'usergamebiodataget@gmail.com',
            firstname: 'UserGame',
            lastname: 'BiodataGet',
            country: 'USA',
            age: 20,
            userGameId: userGameResponse.body.data.id
        });
        const res = await request(app).get(`/api/v1/user_game/biodata/${userGameBiodataResponse.body.data.id}`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        const userGameBiodata = await getDataBySpecificField(UserGameBiodata, 'email')('usergamebiodataget@gmail.com', [{ model: UserGame }]);
        for (const key in userGameBiodata.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'profilePicture') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGameBiodata.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameBiodata.dataValues[key]));
            }

            const userGame = responseData.UserGame;
            if (userGame) {
                expect(userGame).toBeInstanceOf(Object);
                for (const key in userGameBiodata.UserGame.dataValues) {
                    expect(userGame).toHaveProperty(key);
                    expect(JSON.stringify(userGame[key])).toBe(JSON.stringify(userGameBiodata.UserGame.dataValues[key]));
                }
            } else {
                expect(userGame).toBeNull();
            }
        }

        await request(app).delete(`/api/v1/user_game/biodata/${responseData.id}`);
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).get('/api/v1/user_game/biodata/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('404 Not Found', async () => {
        const res = await request(app).get('/api/v1/user_game/biodata/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/biodata/0')} not found`);
    });
});

describe('PATCH /user_game/biodata/:id', () => {
    beforeEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamebiodataupdate',
            password: 'usergameusergamebiodataupdate'
        });
        const userGameBiodataResponse = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'usergamebiodataupdate@gmail.com',
            firstname: 'UserGame',
            lastname: 'BiodataUpdate',
            country: 'USA',
            age: 20,
            userGameId: userGameResponse.body.data.id
        });
        const updatedData = {
            lastname: 'Test 123',
            age: 27
        };
        const userGameBiodata = await getDataBySpecificField(UserGameBiodata, 'email')('usergamebiodataupdate@gmail.com');
        const res = await request(app).patch(`/api/v1/user_game/biodata/${userGameBiodataResponse.body.data.id}`).send(updatedData);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('UserGameBiodata updated successfully');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        expect(responseData).toHaveProperty('before');
        expect(responseData).toHaveProperty('after');
        for (const key of Object.keys(updatedData)) {
            expect(responseData.before).toHaveProperty(key);
            expect(responseData.after).toHaveProperty(key);
            if (key === 'profilePicture') {
                expect(responseData.before[key]).toBe(`${jsonProfilePicturePath}${userGameBiodata.dataValues[key]}`);
                expect(responseData.after[key]).toBe(`${jsonProfilePicturePath}${updatedData[key]}`);
            } else {
                expect(responseData.before[key]).toBe(userGameBiodata[key]);
                expect(responseData.after[key]).toBe(updatedData[key]);
            }
        }

        await request(app).delete(`/api/v1/user_game/biodata/${responseData.after.id}`);
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request (param)', async () => {
        const res = await request(app).patch('/api/v1/user_game/biodata/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (body)', async () => {
        const johnResponse = await request(app).post('/api/v1/user_games').send({
            username: 'johntest',
            password: 'john123'
        });
        const bobResponse = await request(app).post('/api/v1/user_games').send({
            username: 'bobtest',
            password: 'bob123'
        });
        const johnBiodataResponse = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'johntest@gmail.com',
            firstname: 'John',
            lastname: 'Test',
            country: 'USA',
            age: 20,
            userGameId: johnResponse.body.data.id
        });
        const bobBiodataResponse = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'bobtest@gmail.com',
            firstname: 'Bob',
            lastname: 'Test',
            country: 'USA',
            age: 20,
            userGameId: bobResponse.body.data.id
        });
        const res = await request(app).patch(`/api/v1/user_game/biodata/${bobBiodataResponse.body.data.id}`).send({ email: 'johntest@gmail.com' });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }

        await request(app).delete(`/api/v1/user_game/biodata/${bobBiodataResponse.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/biodata/${johnBiodataResponse.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/${bobResponse.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/${johnResponse.body.data.id}`);
    });
    it('404 Not Found', async () => {
        const res = await request(app).patch('/api/v1/user_game/biodata/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/biodata/0')} not found`);
    });
});

describe('DELETE /user_game/biodata/:id', () => {
    beforeEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamebiodatadelete',
            password: 'usergameusergamebiodatadelete'
        });
        const userGameBiodataResponse = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'usergamebiodatadelete@gmail.com',
            firstname: 'UserGame',
            lastname: 'BiodataDelete',
            country: 'USA',
            age: 20,
            userGameId: userGameResponse.body.data.id
        });
        const userGameBiodata = await getDataBySpecificField(UserGameBiodata, 'email')('usergamebiodatadelete@gmail.com');
        const res = await request(app).delete(`/api/v1/user_game/biodata/${userGameBiodataResponse.body.data.id}`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('UserGameBiodata deleted successfully');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        for (const key in userGameBiodata.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'profilePicture') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGameBiodata.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameBiodata.dataValues[key]));
            }
        }

        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).delete('/api/v1/user_game/biodata/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('404 Not Found', async () => {
        const res = await request(app).delete('/api/v1/user_game/biodata/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/biodata/0')} not found`);
    });
});

describe('/user_game/biodata/:id Method Not Allowed', () => {
    it('POST /user_game/biodata/:id', async () => {
        const res = await request(app).post('/api/v1/user_game/biodata/1');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method POST not allowed at endpoint ${getEndpoint('/api/v1/user_game/biodata/1')}`);
    });
    it('PUT /user_game/biodata/:id', async () => {
        const res = await request(app).put('/api/v1/user_game/biodata/1');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_game/biodata/1')}`);
    });
});

describe('GET /user_game/:userGameId/biodata', () => {
    beforeEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameusergamebiodatagetusergameid',
            password: 'usergameusergamebiodatagetusergameid'
        });
        const userGameBiodataResponse = await request(app).post('/api/v1/user_games/biodatas').send({
            email: 'usergamebiodatagetusergameid@gmail.com',
            firstname: 'UserGame',
            lastname: 'BiodataGetUserGameId',
            country: 'USA',
            age: 20,
            userGameId: userGameResponse.body.data.id
        });
        const res = await request(app).get(`/api/v1/user_game/${userGameResponse.body.data.id}/biodata`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        const userGameBiodata = await getDataBySpecificField(UserGameBiodata, 'userGameId')(userGameResponse.body.data.id);
        for (const key in userGameBiodata.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key === 'profilePicture') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGameBiodata.dataValues[key]}`));
            } else {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGameBiodata.dataValues[key]));
            }
        }

        await request(app).delete(`/api/v1/user_game/biodata/${userGameBiodataResponse.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).get('/api/v1/user_game/a/biodata');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
});

describe('/user_game/:userGameId/biodata Method Not Allowed', () => {
    it('POST /user_game/:userGameId/biodata', async () => {
        const res = await request(app).post('/api/v1/user_game/1/biodata');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method POST not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/biodata')}`);
    });
    it('PUT /user_game/:userGameId/biodata', async () => {
        const res = await request(app).put('/api/v1/user_game/1/biodata');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/biodata')}`);
    });
    it('PATCH /user_game/:userGameId/biodata', async () => {
        const res = await request(app).patch('/api/v1/user_game/1/biodata');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/biodata')}`);
    });
    it('DELETE /user_game/:userGameId/biodata', async () => {
        const res = await request(app).delete('/api/v1/user_game/1/biodata');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/user_game/1/biodata')}`);
    });
});
