const request = require('supertest');
const app = require('../../app');
const { baseUrl, testingTimeout } = require('../../config/constants');
const { UserGame, UserGameBiodata, UserGameHistory } = require('../../database/models');
const { getDataBySpecificField, getEndpoint } = require('../../helper');

process.env.NODE_ENV = 'test';
jest.setTimeout(testingTimeout);
const jsonProfilePicturePath = `${baseUrl}/uploads/profiles/`;
const jsonGameCoverPath = `${baseUrl}/uploads/games/`;

describe('GET /user_games', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGames = await UserGame.findAll({
            include: [
                { model: UserGameBiodata },
                { model: UserGameHistory }
            ]
        });
        const res = await request(app).get('/api/v1/user_games');
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');
        expect(res.body.count).toBe(userGames.length);

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Array);
        expect(responseData).toHaveLength(userGames.length);
        for (let i = 0; i < userGames.length; i++) {
            for (const key in userGames[i].dataValues) {
                expect(responseData[i]).toHaveProperty(key);
                if (key !== 'UserGameBiodatum' && key !== 'UserGameHistories') {
                    expect(JSON.stringify(responseData[i][key])).toBe(JSON.stringify(userGames[i].dataValues[key]));
                }
            }

            const biodata = responseData[i].UserGameBiodatum;
            const histories = responseData[i].UserGameHistories;
            if (biodata) {
                expect(biodata).toBeInstanceOf(Object);
                for (const key in userGames[i].UserGameBiodatum.dataValues) {
                    expect(biodata).toHaveProperty(key);
                    if (key === 'profilePicture') {
                        expect(JSON.stringify(biodata[key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGames[i].UserGameBiodatum.dataValues[key]}`));
                    } else {
                        expect(JSON.stringify(biodata[key])).toBe(JSON.stringify(userGames[i].UserGameBiodatum.dataValues[key]));
                    }
                }
            } else {
                expect(biodata).toBeNull();
            }

            expect(histories).toBeInstanceOf(Array);
            expect(histories).toHaveLength(userGames[i].UserGameHistories.length);
            if (histories.length > 0 && histories.length === userGames[i].UserGameHistories.length) {
                for (let j = 0; j < histories.length; j++) {
                    expect(histories[j]).toBeInstanceOf(Object);
                    for (const key in userGames[i].UserGameHistories[j].dataValues) {
                        expect(histories[j]).toHaveProperty(key);
                        if (key === 'cover') {
                            expect(JSON.stringify(histories[j][key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGames[i].UserGameHistories[j].dataValues[key]}`));
                        } else {
                            expect(JSON.stringify(histories[j][key])).toBe(JSON.stringify(userGames[i].UserGameHistories[j].dataValues[key]));
                        }
                    }
                }
            }
        }
    });
});

describe('POST /user_games', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('201 Created', async () => {
        const res = await request(app).post('/api/v1/user_games').send({
            username: 'usergamecreate',
            password: 'usergamecreate'
        });
        const userGame = await getDataBySpecificField(UserGame, 'username')('usergamecreate');
        expect(res.statusCode).toBe(201);
        expect(res.body.statusCode).toBe(201);
        expect(res.body.message).toBe('UserGame created successfully');

        const responseData = res.body.data;
        for (const key in userGame.dataValues) {
            expect(responseData).toHaveProperty(key);
            expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGame.dataValues[key]));
        }

        await request(app).delete(`/api/v1/user_game/${responseData.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).post('/api/v1/user_games').send({ username: '' });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (Username Already Exists)', async () => {
        const res = await request(app).post('/api/v1/user_games').send({
            username: 'bob',
            password: '123'
        });
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
});

describe('/user_games Method Not Allowed', () => {
    it('PUT /user_games', async () => {
        const res = await request(app).put('/api/v1/user_games');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_games')}`);
    });
    it('PATCH /user_games', async () => {
        const res = await request(app).patch('/api/v1/user_games');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PATCH not allowed at endpoint ${getEndpoint('/api/v1/user_games')}`);
    });
    it('DELETE /user_games', async () => {
        const res = await request(app).delete('/api/v1/user_games');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method DELETE not allowed at endpoint ${getEndpoint('/api/v1/user_games')}`);
    });
});

describe('GET /user_game/:id', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameget',
            password: 'usergameget'
        });
        const userGame = await getDataBySpecificField(UserGame, 'id')(
            +userGameResponse.body.data.id, [
            { model: UserGameBiodata },
            { model: UserGameHistory }
        ]);
        const res = await request(app).get(`/api/v1/user_game/${userGameResponse.body.data.id}`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('OK');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        for (const key in userGame.dataValues) {
            expect(responseData).toHaveProperty(key);
            if (key !== 'UserGameBiodatum' && key !== 'UserGameHistories') {
                expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGame.dataValues[key]));
            }
        }

        const biodata = responseData.UserGameBiodatum;
        const histories = responseData.UserGameHistories;
        if (biodata) {
            expect(biodata).toBeInstanceOf(Object);
            for (const key in userGame.UserGameBiodatum.dataValues) {
                expect(biodata).toHaveProperty(key);
                if (key === 'profilePicture') {
                    expect(JSON.stringify(biodata[key])).toBe(JSON.stringify(`${jsonProfilePicturePath}${userGame.UserGameBiodatum.dataValues[key]}`));
                } else {
                    expect(JSON.stringify(biodata[key])).toBe(JSON.stringify(userGame.UserGameBiodatum.dataValues[key]));
                }
            }
        } else {
            expect(biodata).toBeNull();
        }

        expect(histories).toBeInstanceOf(Array);
        expect(histories).toHaveLength(userGame.UserGameHistories.length);
        if (histories.length > 0 && histories.length === userGame.UserGameHistories.length) {
            for (let i = 0; i < histories.length; i++) {
                expect(histories[i]).toBeInstanceOf(Object);
                for (const key in userGame.UserGameHistories[i].dataValues) {
                    expect(histories[i]).toHaveProperty(key);
                    if (key === 'cover') {
                        expect(JSON.stringify(histories[i][key])).toBe(JSON.stringify(`${jsonGameCoverPath}${userGame.UserGameHistories[i].dataValues[key]}`));
                    } else {
                        expect(JSON.stringify(histories[i][key])).toBe(JSON.stringify(userGame.UserGameHistories[i].dataValues[key]));
                    }
                }
            }
        }

        await request(app).delete(`/api/v1/user_game/${responseData.id}`);
    });
    it('400 Bad Request', async () => {
        const res = await request(app).get('/api/v1/user_game/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('404 Not Found', async () => {
        const res = await request(app).get('/api/v1/user_game/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/0')} not found`);
    });
});

describe('PATCH /user_game/:id', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergameupdate',
            password: 'usergameupdate'
        });
        const userGame = await getDataBySpecificField(UserGame, 'username')('usergameupdate');
        const updatedData = { password: 'usergameupdatepatch' }
        const res = await request(app).patch(`/api/v1/user_game/${userGameResponse.body.data.id}`).send(updatedData);

        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('UserGame updated successfully');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        expect(responseData).toHaveProperty('before');
        expect(responseData).toHaveProperty('after');
        for (const key of Object.keys(updatedData)) {
            expect(responseData.before).toHaveProperty(key);
            expect(responseData.after).toHaveProperty(key);
            expect(responseData.before[key]).toBe(userGame[key]);
            if (key !== 'password') {
                expect(responseData.after[key]).toBe(updatedData[key]);
            }
        }

        await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
    });
    it('400 Bad Request (param)', async () => {
        const res = await request(app).patch('/api/v1/user_game/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('400 Bad Request (body)', async () => {
        const johnResponse = await request(app).post('/api/v1/user_games').send({
            username: 'johntest',
            password: 'john123'
        });
        const bobResponse = await request(app).post('/api/v1/user_games').send({
            username: 'bobtest',
            password: 'bob123'
        });
        const res = await request(app).patch(`/api/v1/user_game/${johnResponse.body.data.id}`).send({ username: 'bobtest' })
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }

        await request(app).delete(`/api/v1/user_game/${johnResponse.body.data.id}`);
        await request(app).delete(`/api/v1/user_game/${bobResponse.body.data.id}`);
    });
    it('404 Not Found', async () => {
        const res = await request(app).patch('/api/v1/user_game/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/0')} not found`);
    });
});

describe('DELETE /user_game/:id', () => {
    beforeEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });
    afterEach(async () => {
        await UserGameHistory.destroy({ where: {} });
        await UserGameBiodata.destroy({ where: {} });
        await UserGame.destroy({ where: {} });
    });

    it('200 OK', async () => {
        const userGameResponse = await request(app).post('/api/v1/user_games').send({
            username: 'usergamedelete',
            password: 'usergamedelete'
        });
        const userGame = await getDataBySpecificField(UserGame, 'username')('usergamedelete');
        const res = await request(app).delete(`/api/v1/user_game/${userGameResponse.body.data.id}`);
        expect(res.statusCode).toBe(200);
        expect(res.body.statusCode).toBe(200);
        expect(res.body.message).toBe('UserGame deleted successfully');

        const responseData = res.body.data;
        expect(responseData).toBeInstanceOf(Object);
        for (const key in userGame.dataValues) {
            expect(responseData).toHaveProperty(key);
            expect(JSON.stringify(responseData[key])).toBe(JSON.stringify(userGame.dataValues[key]));
        }
    });
    it('400 Bad Request', async () => {
        const res = await request(app).delete('/api/v1/user_game/a');
        expect(res.statusCode).toBe(400);
        expect(res.body.statusCode).toBe(400);
        expect(res.body.errors).toBeInstanceOf(Array);
        for (const error of res.body.errors) {
            expect(error).toBeInstanceOf(Object);
            expect(error).toHaveProperty('msg');
            expect(error).toHaveProperty('param');
            expect(error).toHaveProperty('location');
        }
    });
    it('404 Not Found', async () => {
        const res = await request(app).delete('/api/v1/user_game/0');
        expect(res.statusCode).toBe(404);
        expect(res.body.statusCode).toBe(404);
        expect(res.body.message).toBe(`Endpoint ${getEndpoint('/api/v1/user_game/0')} not found`);
    });
});

describe('/user_game/:id Method Not Allowed', () => {
    it('POST /user_game/:id', async () => {
        const res = await request(app).post('/api/v1/user_game/1');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method POST not allowed at endpoint ${getEndpoint('/api/v1/user_game/1')}`);
    });
    it('PUT /user_game/:id', async () => {
        const res = await request(app).put('/api/v1/user_game/1');
        expect(res.statusCode).toBe(405);
        expect(res.body.statusCode).toBe(405);
        expect(res.body.message).toBe(`Method PUT not allowed at endpoint ${getEndpoint('/api/v1/user_game/1')}`);
    });
});
